# ZavrsniRad

Developed with Unreal Engine 5

This paper summarizes basic approach to simulation of a four-legged model movement. Animals that belong to that group, as well as their ways of movement, are presented. The animations were made in Blender and imported in Unreal Engine tool, within which the transitions between separate animations and general behavior of models were set. Two examples of four-legged animals were taken: the wolf and the giraffe, and animations were tested over their models.