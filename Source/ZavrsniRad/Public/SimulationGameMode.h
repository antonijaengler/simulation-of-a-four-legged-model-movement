// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SimulationGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ZAVRSNIRAD_API ASimulationGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
};
