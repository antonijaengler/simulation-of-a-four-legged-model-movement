// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GiraffeCharacter.generated.h"

UCLASS()
class ZAVRSNIRAD_API AGiraffeCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AGiraffeCharacter();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isCanterGiraffe;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isGallopGiraffe;
	float movementSpeed;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	// Move forward / backward
	void MoveForwardBackward(float axisValue);
	void MoveRightLeft(float axisValue);
	void Canter();
	void StopCanter();
	void Gallop();
	void StopGallop();
};
