// Fill out your copyright notice in the Description page of Project Settings.


#include "GiraffeCharacter.h"

// Sets default values
AGiraffeCharacter::AGiraffeCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	isCanterGiraffe = false;
	isGallopGiraffe = false;
	movementSpeed = 0.2;
}

// Called when the game starts or when spawned
void AGiraffeCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGiraffeCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AGiraffeCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis(TEXT("MoveForwardBackward"), this, &AGiraffeCharacter::MoveForwardBackward);
	PlayerInputComponent->BindAxis(TEXT("MoveRightLeft"), this, &AGiraffeCharacter::MoveRightLeft);
	PlayerInputComponent->BindAxis(TEXT("LookUpDown"), this, &AGiraffeCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("TurnRightLeft"), this, &AGiraffeCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAction(TEXT("Canter"), IE_Pressed, this, &AGiraffeCharacter::Canter);
	PlayerInputComponent->BindAction(TEXT("Canter"), IE_Released, this, &AGiraffeCharacter::StopCanter);
	PlayerInputComponent->BindAction(TEXT("Gallop"), IE_Pressed, this, &AGiraffeCharacter::Gallop);
	PlayerInputComponent->BindAction(TEXT("Gallop"), IE_Released, this, &AGiraffeCharacter::StopGallop);

}

void AGiraffeCharacter::MoveForwardBackward(float axisValue)
{
	AddMovementInput(GetActorForwardVector() * movementSpeed * axisValue);
}

void AGiraffeCharacter::MoveRightLeft(float axisValue)
{
	AddMovementInput(GetActorRightVector() * movementSpeed * axisValue);
}

void AGiraffeCharacter::Canter()
{
	movementSpeed = 0.4;
	isCanterGiraffe = true;
}

void AGiraffeCharacter::StopCanter()
{
	movementSpeed = 0.1;
	isCanterGiraffe = false;
}

void AGiraffeCharacter::Gallop()
{
	movementSpeed = 0.6;
	isGallopGiraffe = true;
}

void AGiraffeCharacter::StopGallop()
{
	movementSpeed = 0.1;
	isGallopGiraffe = false;
}

