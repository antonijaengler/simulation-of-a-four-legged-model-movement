// Fill out your copyright notice in the Description page of Project Settings.


#include "WolfCharacter.h"

// Sets default values
AWolfCharacter::AWolfCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	isCanter = false;
	isGallop = false;
	movementSpeed = 0.1;

}

// Called when the game starts or when spawned
void AWolfCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWolfCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AWolfCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis(TEXT("MoveForwardBackward"), this, &AWolfCharacter::MoveForwardBackward);
	PlayerInputComponent->BindAxis(TEXT("MoveRightLeft"), this, &AWolfCharacter::MoveRightLeft);
	PlayerInputComponent->BindAxis(TEXT("LookUpDown"), this, &AWolfCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("TurnRightLeft"), this, &AWolfCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAction(TEXT("Canter"), IE_Pressed, this, &AWolfCharacter::Canter);
	PlayerInputComponent->BindAction(TEXT("Canter"), IE_Released, this, &AWolfCharacter::StopCanter);
	PlayerInputComponent->BindAction(TEXT("Gallop"), IE_Pressed, this, &AWolfCharacter::Gallop);
	PlayerInputComponent->BindAction(TEXT("Gallop"), IE_Released, this, &AWolfCharacter::StopGallop);

}

void AWolfCharacter::MoveForwardBackward(float axisValue)
{
	AddMovementInput(GetActorForwardVector() * movementSpeed * axisValue);
}

void AWolfCharacter::MoveRightLeft(float axisValue) 
{
	AddMovementInput(GetActorRightVector() * movementSpeed * axisValue);
}

void AWolfCharacter::Canter()
{
	movementSpeed = 0.4;
	isCanter = true;
}

void AWolfCharacter::StopCanter()
{
	movementSpeed = 0.1;
	isCanter = false;
}

void AWolfCharacter::Gallop()
{
	movementSpeed = 0.6;
	isGallop = true;
}

void AWolfCharacter::StopGallop()
{
	movementSpeed = 0.1;
	isGallop = false;
}

